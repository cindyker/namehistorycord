package com.minecats.cindyk.namehistorycord;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by cindy on 4/6/14.
 */
public class NameHistoryCord extends Plugin implements Listener {

    private MiniConnectionPoolManager pool;
    PlayerListener pl;
    PlayerQueries plQueries;
    NHConfig nhConfig = null;

    @Override
    public void onEnable() {

        pl = new PlayerListener(this);
        plQueries = new PlayerQueries(this);

        getLogger().info("[NameHistoryCord] Before Config Initialization");
        try {

           nhConfig = new NHConfig(this);
            nhConfig.init();
            nhConfig.load();

        } catch(InvalidConfigurationException ex) {

            getLogger().info("[NameHistoryCord] Your Config YML was wrong");
            ex.printStackTrace();
        }

        this.getProxy().getPluginManager().registerListener(this,pl);
        this.getProxy().getPluginManager().registerListener(this,this);

        //Setup SQL
        try{
            connect();
        }catch(ClassNotFoundException cex)
        {
            this.getLogger().log(Level.SEVERE," Error:" + cex.getMessage());
        }
        catch(SQLException ex)
        {
            this.getLogger().log(Level.SEVERE," Error:" + ex.getMessage());
        }

        //Get Config,
        if(nhConfig.Enabled==false)
        {
            getLogger().info("WARNING!! you have NOT setup SQL yet in the Config. Its required.");

        }
        else
        {
            getLogger().info("=^..^=  Setting Up SQL");
            setUpSQL();
        }



        getLogger().info("[NameHistoryCord] is Enabled!");


    }


    @EventHandler
    public void onChat(ChatEvent event) {



        ProxiedPlayer curr_player = ProxyServer.getInstance().getPlayer(event.getSender().toString());

        if(event.isCommand())
        {
            if(event.getMessage().startsWith("/history"))
            {
               // this.getLogger().info("History Command is Called!");
                String delims = "[ ]+";
                String[] tokens = event.getMessage().split(delims);

                try {
                    if( handleCommands(curr_player,tokens) )
                    {
                        event.setCancelled(true);
                    }

                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return;
    }


    private boolean handleCommands(ProxiedPlayer sender, String[] args) throws ClassNotFoundException
    {


        if (!sender.hasPermission("namehistory.command"))
        {

                sender.sendMessage("[NameHistory] You do not have permission to run this command.");

            return true;
        }
        else
        {
            //Select playername, dateadded, lastseen from namehistory where uuid = player.uuid.tostring();
            if(args.length == 3 )
            {

                switch(args[1])
                {
                    case "name":
                        plQueries.getNameInfo(args[2], sender);

                        break;

                    case "player":

                        if( this.getProxy().getPlayer(args[2]) != null)
                            plQueries.getPlayerInfo(this.getProxy().getPlayer(args[2]),  sender);
                        else
                            sender.sendMessage("Player " + args[2] + " has to be online for this query.");
                        break;
                    case "uuid":
                    {
                        if(nhConfig.UUID_Dashes)
                        {
                            if(!args[2].contains("-"))
                            {
                                sender.sendMessage("UUID needs to have dashes in it. " );
                                return true;
                            }

                        }
                        else
                        {
                            if(args[2].contains("-"))
                            {
                                sender.sendMessage("UUID can't have dashes in it. " );
                                return true;
                            }

                        }

                        plQueries.getUUIDInfo(args[2], sender);
                        return true;
                    }


                }
            }
            else
            {
                sender.sendMessage("/history [name/player/uuid] [playername/uuid] ");
                sender.sendMessage(" for player, the player has to be online. ");
            }

        }

        return true;
    }

    private void setUpSQL()
    {

        this.getLogger().info("Connecting to SQL database!");
        {
            Connection connection = null;
            Statement st = null;
            int rs = 0;
            try
            {
                connection = pool.getValidConnection();
                if(connection==null || connection.isClosed())
                {
                    this.getLogger().info("ERROR Connecting to the DATABASE! Please Check it!");
                }
                       rs = st.executeUpdate("CREATE TABLE IF NOT EXISTS "
                        + nhConfig.TablePrefix
                        + "( uuid VARCHAR(36) NOT NULL, playername VARCHAR(32), dateadded DATETIME, lastseen DATETIME, PRIMARY KEY (uuid,playername))");
                this.getLogger().info("SQL database connected!");

                connection.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                this.getLogger().log(Level.SEVERE," Error:" + rs);
            }
            catch(MiniConnectionPoolManager.TimeoutException te)
            {

                getLogger().info("SQL Timeout Exception occurred! Check your SQL Setup!");
                getLogger().log(Level.SEVERE,"Timeout Exception Error:" + te.getMessage());
            }
        }
        return;
    }

    public synchronized void connect() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        getLogger().info("MySQL driver loaded");
        MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
        dataSource.setDatabaseName(nhConfig.Database);
        dataSource.setServerName(nhConfig.Server);
        dataSource.setPort(3306);
        dataSource.setUser(nhConfig.User);
        dataSource.setPassword(nhConfig.Password);
        pool = new MiniConnectionPoolManager(dataSource,20,1);
        getLogger().info("Connection pool ready");

    }

    public void addData(String name, String strUUID ) throws ClassNotFoundException
    {
        PreparedStatement pst = null;
        Connection con = null;
        Statement uuidstmt = null;
        ResultSet rs = null;
        // int num = 1;


        if(strUUID == null)
        {
            getLogger().info("Player UUID is Missing!! " + name);
            return;
        }


        try
        {


            con = pool.getValidConnection();

            if(con == null || con.isClosed())
            {
                getLogger().info("[NameHistoryCord] ERROR With Database Setup!!");
                return;
            }

            String database = nhConfig.TablePrefix;

            getLogger().info("Looking for UUID :  " + strUUID );
            uuidstmt = con.createStatement();
            if(uuidstmt.execute("SELECT * FROM " + database + " wHERe uuid = '" +strUUID+"'"))
            {
                rs = uuidstmt.getResultSet();
                boolean bFound = false;

                java.util.Date today = new java.util.Date();
                java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());


                while (rs.next())
                // if (!rs.next())
                {


                    if(rs.getString("playername").compareToIgnoreCase(name)==0)
                    {
                        bFound = true;
                        //HAS A NEW NAME!
                        //TELL PLAYER THEY ARE SCREWED!
                      //  getLogger().info("Player: " + name + " has been here before.");

                        //Update LastSeen!


                        //INSERT INTO DATABASE.
                        pst = con.prepareStatement("Update " + database+ " Set lastseen = ? where (uuid = ? and playername = ? ) ");

                        pst.setTimestamp(1, timestamp);
                        pst.setString(2, strUUID);
                        pst.setString(3, name);

                        pst.executeUpdate();
                        getLogger().info("Updated Player");


                    }

                }

                if(!bFound)
                {

                    //INSERT INTO DATABASE.
                    pst = con.prepareStatement("INSERT INTO " + database
                            + "(uuid,playername,dateadded,lastseen) VALUES(?, ? , ? , ?)");


                    pst.setString(1, strUUID);
                    pst.setString(2, name);
                    pst.setTimestamp(3, timestamp);
                    pst.setTimestamp(4, timestamp);

                    pst.executeUpdate();
                    getLogger().info("Inserted Player");
                    //System.out.print("inserted");
                }
            }


        }
        catch (SQLException ex)
        {
            System.out.print(ex);
        }
        catch (MiniConnectionPoolManager.TimeoutException tex)
        {
            getLogger().info("SQL Timeout Exception occurred! Check your SQL Setup!");
            System.out.print(tex);
        }
        finally {
            close(pst);
            close(rs);
            close(uuidstmt);
            close(con);
          //  close();
        }
    }
    public synchronized void close() {
        try {
            pool.dispose();
        } catch (SQLException ex) {
            getLogger().info(ex.getMessage());
        }
    }

    public void reload() {
    }

    public  void close(PreparedStatement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public  void close(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public  void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public  void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }



}
