package com.minecats.cindyk.namehistorycord;

import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by cindy on 4/6/14.
 */
public class PlayerListener implements Listener{

    NameHistoryCord plugin;

    PlayerListener(NameHistoryCord plugin)
    {
        this.plugin = plugin;

    }


    @EventHandler
    public void playerLogin(PostLoginEvent event) {

        //sync event for 10 seconds from now...

        if(plugin.nhConfig.Enabled)
        {

            plugin.getLogger().info("Player Login : " + event.getPlayer().getName() + " uuid: " + event.getPlayer().getUniqueId().toString());
            try
            {
                String strUUID ="";

                if(plugin.nhConfig.UUID_Dashes)
                    strUUID = event.getPlayer().getUniqueId().toString();
                else
                    strUUID = event.getPlayer().getUniqueId().toString().replace("-","");

                plugin.addData(event.getPlayer().getName(),strUUID);
            }
            catch(ClassNotFoundException ex)
            {
                plugin.getLogger().info(ex.getMessage());
            }
        }
        else
        {
            plugin.getLogger().info("WARNING NameHistoryCord config has not been set! FIX IT!");
        }



    }




}
