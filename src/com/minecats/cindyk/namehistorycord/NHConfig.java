package com.minecats.cindyk.namehistorycord;

/**
 * Created by cindy on 4/6/14.
 */

import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.Comments;
import net.cubespace.Yamler.Config.Config;

import java.io.File;

public class NHConfig extends  net.cubespace.Yamler.Config.Config{

    public NHConfig(NameHistoryCord plugin) {

        plugin.getLogger().info("[NameHistoryCord] Config Constructor");

        CONFIG_HEADER = new String[]{"Configuration of NameHistoryCord"};
        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");
    }

    /*
    @Comments({
            "This is the URL of the Database",
            "Must be jdbc:<database engine>:<connection parameter>",
            "For H2 (which is the default file based DB): jdbc:h2:{DIR}thesuit.db",
            "For MySQL: jdbc:mysql://<host>:<port>/<database>"
    })
    private String Url = "jdbc:h2:{DIR}thesuit.db";
*/
    @Comment("Enabled")
    public Boolean Enabled = false;

    @Comment("The Server the Database is on")
    public String Server = "localhost";

    @Comment("The Database Name")
    public String Database = "NameHistory";

    @Comment("The Username which should be used to auth against the Database")
    public String User = "usertest";

    @Comment("The Password for the User")
    public String Password = "passtest";

    @Comment("The Table name that will get created in the Database")
    public String TablePrefix = "history";

    @Comment("Logging Level - info or debut")
    public String LogLevel = "info";

    @Comment("Do you want Dashes? Mojang puts in dashes in the UUID's by default. IF you switch you need to FIX your database or wipe it and start fresh.")
    public boolean UUID_Dashes = true;
}

